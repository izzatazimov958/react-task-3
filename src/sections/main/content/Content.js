import CardComponent from "../../../components/CardComponent";
import React, { useEffect, useState } from "react";
import axios from "axios";
import "./content.scss";

const Content = () => {
  const moviesUrl = `https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/movies`;
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    const getMovies = async () => {
      await axios
        .get(moviesUrl)
        .then((res) => setMovies(res.data))
        .catch((err) => console.log(err));
    };
    getMovies();
  }, [moviesUrl]);

  return (
    <section className="content">
      <div className="main-banner">
        {movies.map(
          (i) =>
            i.type === "banner" && (
              <img
                src={i.movieImageUrl}
                alt={i.type}
                className="img"
                key={i.id}
              />
            )
        )}
      </div>
      <p className="text">Continue Watching | 4 Movies</p>
      <div className="card-wrap">
        {movies.map(
          (movie) =>
            movie &&
            movie.type !== "banner" && (
              <CardComponent
                title={movie.title}
                date={movie.release_date}
                length={movie.duration}
                img={movie.movieImageUrl}
                key={movie.id}
              />
            )
        )}
      </div>
    </section>
  );
};

export default Content;
