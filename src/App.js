import "./App.scss";
import Main from "./sections/main/Main";
import Nav from "./sections/navbar/Nav";

function App() {
  return (
    <div className="App">
      <Nav/>
      <div className="container">
        <Main />
      </div>
    </div>
  );
}

export default App;
