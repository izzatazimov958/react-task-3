// import axios from "axios";
import React  from "react";
import "./comment.scss";

const comments = [
  {
    id: 1,
    username: "username1",
    comment: `Username
      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
  },
  {
    id: 2,
    username: "username2",
    comment: `Username
      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
  },
  {
    id: 3,
    username: "username3",
    comment: `Username
      Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
  },
];

const Comment = () => {
  // const comUrl = `https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/movies/${1}/comments`;
  // const [realCom, setRealCom] = useState([]);

  // const getCom = async () => {
  //   await axios
  //     .get(comUrl)
  //     .then((res) => setRealCom(res.data))
  //     .catch((e) => console.log(e));
  // };

  // useEffect(() => {
  //   getCom();
  // }, []);

  return (
    <section className="comment-section">
      <h3 className="com-title">Comments</h3>
      <div className="comment-box">
        {comments.map((com) => (
          <div className="each-comment" key={com.id}>
            {/* <h6>{com?.username}</h6>
            <p>{com?.comment_msg} </p> */}
            <h6>{com.username}</h6>
            <p>{com.comment} </p>
          </div>
        ))}
      </div>

      <form className="form">
        <input
          type="text"
          className="username"
          placeholder="Username"
          name="username"
        />
        <textarea
          className="comment"
          placeholder="Comment"
          name="comment"
        ></textarea>
        <button className="btn">Post</button>
      </form>
    </section>
  );
};

export default Comment;
