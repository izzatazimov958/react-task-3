import React from "react";
import "./nav.scss";

const nav_menu = [
  {
    id: 1,
    title: "Movies",
    link: "#",
  },
  {
    id: 2,
    title: "TV shows",
    link: "#",
  },
  {
    id: 3,
    title: "Animations",
    link: "#",
  },
];

const nav_icons = [
  {
    icon: "../../images/nav/Search.svg",
  },
  {
    icon: "../../images/nav/Circle.svg",
  },
  {
    icon: "../../images/nav/Dots.svg",
  },
];

const nav_title = "MOVEA";

const Nav = () => {
  return (
    <nav className="nav">
        <div className="container">
      <div className="logo">
        <span>{nav_title}</span>
      </div>
      <ul className="nav-menu">
        {nav_menu.map((li) => (
          <li key={li.id}>
            <a href={li.link}>{li.title}</a>
          </li>
        ))}

      </ul>
      <div className="nav-icons">
        {nav_icons.map((item, idx) => (
          <img src={item.icon} alt="icon" key={idx} />
        ))}
      </div>
    </div>
    </nav>
  );
};

export default Nav;
