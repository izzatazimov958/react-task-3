import React from "react";
import "./cardcomponent.scss";

const CardComponent = (props) => {
  const { title, date, length, img } = props;
  //
  return (
    <div
      style={{
        backgroundImage: `url(${img})`,
        backgroundPosition: "center",
        backgroundSize: "contain",
      }}
      className="card"
    >
      <div className="wrapper">
        <div className="card-info">
          <div className="play-button">
            <img src="../images/nav/Circle.svg" alt="play" />
          </div>
          <div className="texts">
            <p>{title ? title : "title"} </p>
            <span>{date ? date : "date"}</span>
          </div>
        </div>

        <div className="length">
          <span>{length ? length : "00:00:00"}</span>
        </div>
      </div>
    </div>
  );
};

export default CardComponent;
