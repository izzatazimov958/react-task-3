import React from "react";
import Comment from "./comment/Comment";
import Content from "./content/Content";
import './main.scss'

const Main = () => {
  return (
    <div className="main-wrapper">
      <Content />
      <Comment />
    </div>
  );
};

export default Main;
